# :card_file_box: Материалы к вебинару 'Начните работу с Kubernetes: запускаем приложение'

:floppy_disk: В данном репозитории собраны материалы к вебинару,
организованному [Selectel](https://selectel.ru) и проведённому 10.08.2022.

:rocket: Для запуска нагрузки были использованы услуги [Managed Kubernetes](https://selectel.ru/services/cloud/kubernetes)
и [Container Registry](https://selectel.ru/services/cloud/container-registry).

:video_camera: Запись вебинара доступна на **Youtube**:

[![Начните работу с Kubernetes: запускаем приложение и отвечаем на все вопросы про K8s](https://img.youtube.com/vi/ILm5GeQiOhg/0.jpg)](https://youtu.be/ILm5GeQiOhg "Начните работу с Kubernetes: запускаем приложение и отвечаем на все вопросы про K8s")

:page_facing_up: Использованы следующие провайдеры из [registry.terraform.io](https://registry.terraform.io):

- [selectel/selectel](https://registry.terraform.io/providers/selectel/selectel/latest/docs)
- [hashicorp/helm](https://registry.terraform.io/providers/hashicorp/helm/latest/docs)
- [hashicorp/kubernetes](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs)

:link: Также можно найти примеры создания кластера **Managed Kubernetes**
в нашем репозитории [github.com/selectel/terraform-examples](https://github.com/selectel/terraform-examples/tree/master/examples/mks).
