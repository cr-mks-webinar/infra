variable "selectel_token" {
  default   = ""
  sensitive = true
}

variable "cr_token" {
  default   = ""
  sensitive = true
}
