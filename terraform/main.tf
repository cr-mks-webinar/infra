terraform {
  required_providers {
    selectel = {
      source = "selectel/selectel"
      version = ">= 3.8.4"
    }
  }
  required_version = ">= 1.0"
}

# Configure the Selectel Provider
provider "selectel" {
  token = var.selectel_token
}

provider "kubernetes" {
  host                   = data.selectel_mks_kubeconfig_v1.kubeconfig.server
  client_certificate     = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_cert)
  client_key             = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_key)
  cluster_ca_certificate = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.cluster_ca_cert)
}

provider "helm" {
  kubernetes {
    host                   = data.selectel_mks_kubeconfig_v1.kubeconfig.server
    client_certificate     = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_cert)
    client_key             = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_key)
    cluster_ca_certificate = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.cluster_ca_cert)
  }
}

# MKS cluster
resource "selectel_mks_cluster_v1" "cluster" {
  name                              = "webinar-cluster"
  project_id                        = "97d8b07d35bc4e7497e7936e111becc7"
  region                            = "ru-9"
  kube_version                      = "1.24.3"
  zonal                             = true
  enable_autorepair                 = false
  enable_patch_version_auto_upgrade = false
  network_id                        = "38cd8be3-3c0f-492e-920d-d480375fe529"
  subnet_id                         = "2f8d5809-42ac-4d57-b653-e5c1d6c7774a"
}

# MKS node group
resource "selectel_mks_nodegroup_v1" "nodegroup" {
  cluster_id        = selectel_mks_cluster_v1.cluster.id
  project_id        = selectel_mks_cluster_v1.cluster.project_id
  region            = selectel_mks_cluster_v1.cluster.region
  availability_zone = "ru-9a"
  nodes_count       = 2
  keypair_name      = "shatohin"
  cpus              = 2
  ram_mb            = 2048
  volume_gb         = 16
  volume_type       = "fast.ru-9a"
}

data "selectel_mks_kubeconfig_v1" "kubeconfig" {
  cluster_id = selectel_mks_cluster_v1.cluster.id
  project_id = selectel_mks_cluster_v1.cluster.project_id
  region     = selectel_mks_cluster_v1.cluster.region
}

resource "local_file" "foo" {
  content         = data.selectel_mks_kubeconfig_v1.kubeconfig.raw_config
  file_permission = 600
  filename        = "webinar.yaml"
}

resource "selectel_vpc_floatingip_v2" "floatingip_1" {
  project_id = "97d8b07d35bc4e7497e7936e111becc7"
  region     = "ru-9"
}

resource "helm_release" "ingress_nginx" {
  name       = "ingress-nginx"
  namespace  = "ingress"

  create_namespace = true

  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"

  set {
    name  = "controller.service.loadBalancerIP"
    value = selectel_vpc_floatingip_v2.floatingip_1.floating_ip_address
  }

  depends_on = [
    selectel_mks_nodegroup_v1.nodegroup
  ]
}

resource "selectel_domains_domain_v1" "domain_1" {
  name = "shatohin.space"
}

resource "selectel_domains_record_v1" "a_record_1" {
  domain_id = selectel_domains_domain_v1.domain_1.id
  name      = "*.shatohin.space"
  type      = "A"
  content   = selectel_vpc_floatingip_v2.floatingip_1.floating_ip_address
  ttl       = 60
}

resource "kubernetes_namespace" "webinar" {
  metadata {
    name = "webinar"
  }
}

resource "kubernetes_secret" "selectel_cr" {
  metadata {
    name = "selectel-cr"
    namespace = "webinar"
  }

  type = "kubernetes.io/dockerconfigjson"

  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "cr.selcloud.ru" = {
          "username" = "token"
          "password" = var.cr_token
          "auth"     = base64encode("token:${var.cr_token}")
        }
      }
    })
  }
}
